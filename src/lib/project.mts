import cheerio from "cheerio";
import { fromSysPath, join as furiJoin, toSysPath } from "furi";
import sysPath from "path";
import { combineLatest, firstValueFrom, Observable } from "rxjs";
import { map as rxMap,mergeMap as rxMergeMap, switchMap as rxSwitchMap } from "rxjs/operators";
import stripJsonComments from "strip-json-comments";
import url, { URL } from "url";

import { generateWays as chunkGenerateWays } from "./chunk.mjs";
import { compileLevelSets, getJavaLevelSets, LevelSet } from "./compiler/level-set.mjs";
import { LevelPort, LevelRefMap } from "./compiler/links.mjs";
import { ObfFn, resolveObf } from "./compiler/obf.mjs";
import {
  CompilerResult as Cr,
  flattenRx as crFlattenRx,
  join as crJoin,
  joinArray as crJoinArray,
  joinMap as crJoinMap,
  of as crOf,
} from "./compiler-result.mjs";
import { GameData, Links, OutLevelSet, stringifyGameData } from "./game-data.mjs";
import { fromConfig as assetsFromConfig, JavaAssets } from "./java/assets.mjs";
import {
  EfcAssetsConfig,
  EfcConfig,
  EfcLocaleConfig,
  fromPath as javaConfigFromPath,
  ResolvedConfig,
} from "./java/config.mjs";
import { DimensionsConfig, resolveDimensions, Way } from "./java/dimensions.mjs";
import {
  fromPath as javaDimensionsLocaleFromPath,
  JavaDimensionsLocale,
} from "./java/dimensions-locale.mjs";
import { fromJavaDimensions } from "./locales/lands-locale.mjs";
import { Locale } from "./locales/locale.mjs";
import { observeNodes } from "./observable-fs.mjs";
import { readTextFile } from "./utils.mjs";

export interface Compiled {
  content: string;
  locales: Map<string, string>;
}

export function compile(config: EfcConfig): Observable<Cr<Compiled>> {
  const dimensions$: Observable<Cr<DimensionsConfig>> = resolveDimensions(config.dimensions, config.levels);
  const obfFn$: Observable<Cr<ObfFn>> = resolveObf(config.obf);

  return combineLatest([dimensions$, obfFn$])
    .pipe(rxMap(([cr0, cr1]) => crJoinArray([cr0, cr1])))
    .pipe(rxSwitchMap(async (cr: Cr<[DimensionsConfig, ObfFn]>): Promise<Cr<Compiled>> => {
      return cr.flatMapAsync(async ([dimensions, obfFn]: [DimensionsConfig, ObfFn]): Promise<Cr<Compiled>> => {
        return innerCompileFromDirectory(config, dimensions, obfFn);
      });
    }));
}

export async function compileFromDirectory(
  dir: url.URL,
  prettyXml: boolean = false,
): Promise<Cr<Compiled>> {
  const compiled$ = observeCompilationFromDirectory(dir, prettyXml);
  return firstValueFrom(compiled$);
}

export function observeCompilationFromDirectory(
  dir: url.URL,
  prettyOutput: boolean = false,
): Observable<Cr<Compiled>> {
  const javaConfigPath: url.URL = furiJoin(dir, ["config.xml"]);
  return javaConfigFromPath(javaConfigPath).pipe(
    rxSwitchMap((javaConfig: Cr<ResolvedConfig>): Observable<Cr<Compiled>> => {
      return crFlattenRx(
        javaConfig.map(config => compileFromJavaConfig(dir, config, prettyOutput)),
      );
    }),
  );
}

function compileFromJavaConfig(
  dir: url.URL,
  javaConfig: ResolvedConfig,
  prettyOutput: boolean,
): Observable<Cr<Compiled>> {
  const locales: Map<string, EfcLocaleConfig> = new Map();
  for (const [localeId, javaLocaleConfig] of javaConfig.locales) {
    const efcLocaleConfig: EfcLocaleConfig = {
      items: fromSysPath(javaLocaleConfig.items),
      keys: fromSysPath(javaLocaleConfig.keys),
      lands: fromSysPath(javaLocaleConfig.lands),
      statics: fromSysPath(javaLocaleConfig.statics),
    };
    locales.set(localeId, efcLocaleConfig);
  }

  const assets: EfcAssetsConfig = {
    useBaseAssets: javaConfig.assets.useBaseAssets,
    roots: javaConfig.assets.roots.map(fromSysPath),
  };

  const config: EfcConfig = {
    root: dir,
    levels: fromSysPath(javaConfig.levelsDir),
    patcherData: fromSysPath(javaConfig.patcherDataDir),
    scoreItems: fromSysPath(javaConfig.scoreItemsFile),
    specialItems: fromSysPath(javaConfig.specialItemsFile),
    dimensions: fromSysPath(javaConfig.dimensionsFile),
    locales,
    assets,
    prettyOutput,
  };

  return compile(config);
}

async function innerCompileFromDirectory(
  config: EfcConfig,
  javaDimensions: DimensionsConfig,
  obfFn: ObfFn,
): Promise<Cr<Compiled>> {
  return (await getJavaLevelSets(javaDimensions))
    .flatMapAsync(async (javaLevelSets: ReadonlyMap<string, LevelSet>): Promise<Cr<Compiled>> => {
      const assets: Cr<JavaAssets> = await assetsFromConfig(config.assets);
      const levelSets: Cr<ReadonlyMap<string, OutLevelSet>> = assets
        .flatMap(assets => compileLevelSets(javaLevelSets, assets, obfFn));

      const patcherData: Cr<ReadonlyMap<string, unknown>> = crJoinMap(await patcherDataMod.get(config.patcherData));
      const specialItems: Cr<cheerio.Cheerio> = await readXml(config.specialItems, "items");
      const scoreItems: Cr<cheerio.Cheerio> = await readXml(config.scoreItems, "items");

      return LevelRefMap.make(javaLevelSets, javaDimensions.tags).flatMapAsync(async levelRefMap => {
        const links: Cr<Links> = desugarWays(generateWays(javaLevelSets).concat(javaDimensions.ways), levelRefMap)
          .map(ways => ({ways, tags: levelRefMap.tags}));

        const localesMap: Map<string, Cr<Locale>> = new Map();
        for (const [localeId, localeConfig] of config.locales) {
          const javaDimensionsLocale: Cr<JavaDimensionsLocale> = await javaDimensionsLocaleFromPath(localeConfig.lands);
          localesMap.set(
            localeId,
            crJoin({
              statics: await readXml(localeConfig.statics, "statics"),
              items: await readXml(localeConfig.items, "items"),
              lands: javaDimensionsLocale.flatMap(dim => fromJavaDimensions(dim, levelRefMap)),
              keys: await readXml(localeConfig.keys, "keys"),
              quests: crOf(undefined),
              families: crOf(undefined),
            }),
          );
        }
        const locales: Cr<Map<string, Locale>> = crJoinMap(localesMap);

        return crJoin({
          levelSets,
          patcherData,
          links,
          specialItems,
          scoreItems,
          locales,
        }).map((gameData: GameData) => {
          const prettyOutput: boolean = config.prettyOutput !== undefined ? config.prettyOutput : false;
          const content: string = stringifyGameData(gameData, prettyOutput);
          const outLocales: Map<string, string> = new Map();
          return {content, locales: outLocales};
        });
      });

    });
}

function desugarWays(ways: ReadonlyArray<Way>, levelRefMap: LevelRefMap): Cr<ReadonlyArray<Way>> {
  const waysCr: Cr<Way>[] = ways.map(way => {
    const type: Cr<"one-way" | "two-way"> = crOf(way.type);
    const from: Cr<LevelPort> = levelRefMap.desugarWithPort(way.from);
    const to: Cr<LevelPort> = levelRefMap.desugarWithPort(way.to);
    return crJoin({type, from, to});
  });
  return crJoinArray(waysCr);
}

function generateWays(javaLevelSets: ReadonlyMap<string, LevelSet>): ReadonlyArray<Way> {
  const ways: ReadonlyArray<Way>[] = [];
  for (const levelSet of javaLevelSets.values()) {
    for (const chunk of levelSet.chunks) {
      const cur: ReadonlyArray<Way> = chunkGenerateWays(chunk);
      if (cur.length > 0) {
        ways.push(cur);
      }
    }
  }

  return ([] as Way[]).concat(...ways);
}

async function readXml(path: URL, rootName: string): Promise<Cr<cheerio.Cheerio>> {
  return (await readTextFile(path)).map(text => {
    const $: cheerio.Root = cheerio.load(text, {xmlMode: true});
    return $(rootName);
  });
}

namespace patcherDataMod {
  interface State {
    /**
     * Map from absolute paths to versions.
     */
    readonly nodes: ReadonlyMap<string, number>;

    /**
     * Map from keys to JSON values.
     */
    readonly data: ReadonlyMap<string, Cr<unknown>>;
  }

  export function observe(dir: string): Observable<ReadonlyMap<string, Cr<unknown>>> {
    let prev: State | undefined = undefined;

    return observeNodes("*.json", {cwd: dir}).pipe(
      rxMergeMap(async (nodes: ReadonlyMap<string, number>): Promise<ReadonlyMap<string, Cr<unknown>>> => {
        const state: State = await next(nodes, prev);
        prev = state;
        return state.data;
      }),
    );
  }

  export async function get(dirUrl: URL): Promise<ReadonlyMap<string, Cr<unknown>>> {
    const dir: string = toSysPath(dirUrl);
    const nodes$ = observeNodes("*.json", {cwd: dir});
    const nodes: ReadonlyMap<string, number> = await firstValueFrom(nodes$);
    return (await next(nodes)).data;
  }

  // eslint-disable-next-line no-inner-declarations
  async function next(
    nodes: ReadonlyMap<string, number>,
    prev: State | undefined = undefined,
  ): Promise<State> {
    const data: Map<string, Cr<unknown>> = new Map();
    for (const [fsNode, version] of nodes) {
      const key: string = sysPath.basename(fsNode, ".json");
      let value: Cr<unknown>;
      if (prev !== undefined && version === prev.nodes.get(fsNode) && prev.data.has(key)) {
        value = prev.data.get(key)!;
      } else {
        const text: Cr<string> = await readTextFile(fsNode);
        // We accept comments in the patcher's JSON, like in the Java version
        value = text.map(t => JSON.parse(stripJsonComments(t)));
      }
      data.set(key, value);
    }

    return {nodes, data};
  }
}
