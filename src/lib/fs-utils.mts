import { Buffer } from "buffer";
import fs from "fs";
import { fromSysPath, toSysPath } from "furi";
import sysPath from "path";
import url from "url";

export async function outputFileAsync(filePath: url.URL, data: Buffer): Promise<void> {
  await mkdirpAsync(fromSysPath(sysPath.dirname(toSysPath(filePath.toString()))));
  await writeFileAsync(filePath, data);
}

export async function mkdirpAsync(dirPath: url.URL): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.mkdir(dirPath, {recursive: true}, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

export async function writeFileAsync(filePath: url.URL, data: Buffer): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}
