import { findUpSync } from "find-up";
import fs from "fs";
import sysPath from "path";
import { fileURLToPath } from "url";

const cwd: string = sysPath.join(fileURLToPath(import.meta.url), "..");

const packagePath: string | undefined = findUpSync("package.json", {cwd});

if (packagePath === undefined) {
  throw new Error("Cannot find `package.json`");
}

const pkg: any = JSON.parse(fs.readFileSync(packagePath, {encoding: "utf-8"}));

export const VERSION: string = pkg.version;
