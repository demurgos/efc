import { asMap, obfuscate } from "@eternalfest/obf/obf.js";
import { Observable, of as rxOf } from "rxjs";
import { URL } from "url";

import { CompilerResult as Cr, error as crError, of as crOf, rxFlatMapCr } from "../compiler-result.mjs";
import { AnyError } from "../diagnostic/errors.mjs";
import { EfcConfig } from "../java/config.mjs";
import { observeTextFile } from "../utils.mjs";

/**
 * Resolved obfuscation function.
 *
 * Maps the clear variable name to the obfuscated value.
 */
export type ObfFn = (clear: string) => string;

const IDENTITY: ObfFn = (clear: string): string => clear;
const IDENTITY$: Observable<Cr<ObfFn>> = rxOf(crOf(IDENTITY));

export function resolveObf(config: EfcConfig["obf"]): Observable<Cr<ObfFn>> {
  if (config === undefined) {
    return IDENTITY$;
  }
  const key: string | null = config.key;
  let parentMap: Observable<Cr<ReadonlyMap<string, string>>>;
  if (config.parentMap instanceof URL) {
    const parentMapUri: URL = config.parentMap;
    parentMap = observeTextFile(parentMapUri)
      .pipe(rxFlatMapCr((parentMapStr: string): Cr<ReadonlyMap<string, string>> => {
        try {
          return crOf(asMap(JSON.parse(parentMapStr)));
        } catch (e) {
          return crError(new AnyError(`Failed to read parent obf map: ${parentMapUri}`));
        }
      }));
  } else {
    parentMap = rxOf(crOf(config.parentMap));
  }
  return parentMap.pipe(rxFlatMapCr((parentMap: ReadonlyMap<string, string>): Cr<ObfFn> => {
    function obfFn(clear: string): string {
      return obfuscate(clear, key, parentMap);
    }
    return crOf(obfFn);
  }));
}
