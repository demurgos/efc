import { emit as emitMtbon } from "@eternalfest/mtbon";
import fs from "fs";
import { join as furiJoin } from "furi";
import { URL } from "url";

import { CACHE } from "../cache.mjs";
import { Chunk, getChunkLength, GridChunk, LinearChunk, TransientChunk } from "../chunk.mjs";
import {
  CompilerResult as Cr,
  error as crError,
  errors as crErrors,
  joinArray as crJoinArray,
  joinArrayAsync as crJoinArrayAsync,
  joinMap as crJoinMap,
  of as crOf,
} from "../compiler-result.mjs";
import { DUPLICATE_LEVEL_ERROR, INVALID_FILENAME_ERROR } from "../diagnostic/codes.mjs";
import { AnyError } from "../diagnostic/errors.mjs";
import { Diagnostic, DiagnosticCategory } from "../diagnostic/report.mjs";
import { OutLevelSet } from "../game-data.mjs";
import { JavaAssets } from "../java/assets.mjs";
import {
  ChunkConfig,
  DimensionsConfig,
  GridChunkConfig,
  LevelSetConfig,
  LinearChunkConfig,
  TransientChunkConfig,
} from "../java/dimensions.mjs";
import * as java from "../java/level.mjs";
import { readTextFile } from "../utils.mjs";
import { javaToHfLevel } from "./convert.mjs";
import { computeGridInfo, GridChunkInfo } from "./grid.mjs";
import { escapeLevel } from "./hf.mjs";
import { ObfFn } from "./obf.mjs";

export interface LevelSet {
  readonly varName: string;
  readonly did: string | null;
  // Add an empty level at the start
  readonly hasPaddingLevel: boolean;
  readonly ensureEqualLengths: boolean;
  readonly chunks: ReadonlyArray<Chunk>;
}

export function compileLevelSets(
  javaLevelSets: ReadonlyMap<string, LevelSet>,
  assets: JavaAssets,
  obfFn: ObfFn,
): Cr<ReadonlyMap<string, OutLevelSet>> {
  const result: Map<string, Cr<OutLevelSet>> = new Map();
  for (const [setKey, javaLevelSet] of javaLevelSets) {
    const outLevelSet: Cr<OutLevelSet> = compileLevelSet(javaLevelSet, assets, obfFn);
    result.set(setKey, outLevelSet);
  }
  return crJoinMap(result);
}

export async function getJavaLevelSets(
  config: DimensionsConfig,
): Promise<Cr<ReadonlyMap<string, LevelSet>>> {
  const result: Map<string, Cr<LevelSet>> = new Map();
  for (const [setKey, setConfig] of config.sets) {
    const levelSet: Cr<LevelSet> = await getJavaLevelSet(setConfig);
    result.set(setKey, levelSet);
  }
  return crJoinMap(result);
}

async function getJavaLevelSet(config: LevelSetConfig): Promise<Cr<LevelSet>> {
  const chunks: Cr<Chunk>[] = [];
  for (const chunk of config.chunks) {
    chunks.push(await getJavaChunk(chunk));
  }
  return crJoinArray(chunks).map(chunks => ({
    chunks,
    varName: config.varName,
    did: config.did,
    hasPaddingLevel: config.hasPaddingLevel,
    ensureEqualLengths: config.ensureEqualLengths,
  }));
}

async function getJavaChunk(config: ChunkConfig): Promise<Cr<Chunk>> {
  switch (config.type) {
    case "linear":
      return getJavaLinearChunk(config);
    case "transient":
      return getJavaTransientChunk(config);
    case "grid":
      return getJavaGridChunk(config);
    default:
      return crError(new AnyError(`Unexpected chunk type ${(config as any).type}`));
  }
}

async function getJavaLinearChunk(config: LinearChunkConfig): Promise<Cr<LinearChunk>> {
  return (await crJoinArrayAsync(getJavaLevelsFromDir(config.dir))).map(
    (locatedLevels): LinearChunk => {
      const levels: java.JavaLevel[] = [];
      for (const locatedLevel of locatedLevels) {
        levels.push(locatedLevel[1]);
      }
      if (config.emitTag) {
        return {type: "linear", levels, firstLevelTag: config.name};
      } else {
        return {type: "linear", levels};
      }
    },
  );
}

async function getJavaTransientChunk(config: TransientChunkConfig): Promise<Cr<TransientChunk>> {
  const children: Cr<Chunk>[] = [];
  for (const childConfig of config.children) {
    children.push(await getJavaChunk(childConfig));
  }
  return crJoinArray(children).map(children => ({type: "transient" as "transient", children}));
}

const GRID_LEVEL_REGEX: RegExp = /.*\((-?[0-9]+);(-?[0-9]+)\).*\.lvl$/;

async function getJavaGridChunk(config: GridChunkConfig): Promise<Cr<GridChunk>> {
  return (await crJoinArrayAsync(getJavaLevelsFromDir(config.dir))).flatMap(
    (locatedLevels): Cr<GridChunk> => {
      const errors: Diagnostic[] = [];
      const grid: Map<number, Map<number, java.JavaLevel>> = new Map();

      for (const [fileUri, level] of locatedLevels) {
        const match: RegExpMatchArray | null = GRID_LEVEL_REGEX.exec(fileUri.pathname);
        if (match === null) {
          errors.push({
            code: INVALID_FILENAME_ERROR,
            category: DiagnosticCategory.Error,
            message: "The filename isn't a valid grid level name",
            fileUri: fileUri.toString(),
          });
          continue;
        }
        const x: number = parseInt(match[1], 10)!;
        const y: number = parseInt(match[2], 10)!;

        let col: Map<number, java.JavaLevel> | undefined = grid.get(x);
        if (col === undefined) {
          col = new Map();
          grid.set(x, col);
        }

        if (col.has(y)) {
          errors.push({
            code: DUPLICATE_LEVEL_ERROR,
            category: DiagnosticCategory.Error,
            message: `The level (${x};${y}) already exists`,
            fileUri: fileUri.toString(),
          });
          continue;
        }
        col.set(y, level);
      }

      if (errors.length !== 0) {
        return crErrors(errors);
      }

      return crOf({
        type: "grid" as "grid",
        firstLevelTag: config.name,
        levels: grid,
      });
    },
  );

}

async function* getJavaLevelsFromDir(dir: URL): AsyncIterable<Cr<[URL, java.JavaLevel]>> {
  for await(const fileUri of getLevelFilesFromDir(dir)) {
    yield crJoinArray([
      crOf(fileUri),
      (await readTextFile(fileUri))
        .flatMap(java.parseString)
        .mapDiagnostics(value => ({
          ...value,
          message: value.message,
          fileUri,
        })),
    ]);
  }
}

async function* getLevelFilesFromDir(dir: URL): AsyncIterable<URL> {
  let names: string[];
  try {
    names = await fs.promises.readdir(dir);
    // Force a consistent order on the names
    names.sort();
  } catch (err) {
    if ((err instanceof Error) && (err as NodeJS.ErrnoException).code === "ENOENT") {
      // Treat missing directories as empty
      return;
    } else {
      throw err;
    }
  }
  for (const name of names) {
    if (!isLvlFileName(name)) {
      continue;
    }
    const resolved: URL = furiJoin(dir, name);
    const stats: fs.Stats = await fs.promises.stat(resolved);
    if (!stats.isFile()) {
      continue;
    }
    yield resolved;
  }
}

function isLvlFileName(fileName: string): boolean {
  return /\.lvl$/.test(fileName);
}

export function compileLevelSet(levelSet: LevelSet, assets: JavaAssets, obfFn: ObfFn): Cr<OutLevelSet> {
  return crJoinArray(compileAllLevels(levelsInLevelSet(levelSet), assets))
    .map(levels => ({varName: obfFn(levelSet.varName), mtbon: levels.join(":")}));
}

function* levelsInLevelSet(levelSet: LevelSet): Iterable<java.JavaLevel | null> {
  let minimumLength: number = 0;
  if (levelSet.ensureEqualLengths) {
    for (const chunk of levelSet.chunks) {
      minimumLength = Math.max(minimumLength, getChunkLength(chunk));
    }
  }

  let emitSeparator: boolean = levelSet.hasPaddingLevel;

  for (const chunk of levelSet.chunks) {
    if (emitSeparator) {
      yield null;
    }
    emitSeparator = true;

    yield* padIterable(levelsInChunk(chunk), minimumLength, null);
  }
}

function* padIterable<T>(it: Iterable<T>, minLen: number, padding: T): Iterable<T> {
  for (const e of it) {
    yield e;
    minLen--;
  }
  while (minLen-- > 0) {
    yield padding;
  }
}

function* levelsInChunk(chunk: Chunk): Iterable<java.JavaLevel | null> {
  switch (chunk.type) {
    case "linear": {
      yield* chunk.levels;
      break;
    }
    case "transient": {
      for (const child of chunk.children) {
        yield* levelsInChunk(child);
      }
      break;
    }
    case "grid": {
      const info: GridChunkInfo = CACHE.get(chunk, c => computeGridInfo(c.levels));
      for (const level of info.levels) {
        if (level === null) {
          yield null;
        } else {
          yield level.level;
        }
      }
      break;
    }
    default:
      yield chunk;
  }
}

export function compileLevel(javaLevel: java.JavaLevel, assets: JavaAssets): Cr<string> {
  return javaToHfLevel(javaLevel, assets)
    .map(escapeLevel)
    .map(emitMtbon);
}

function* compileAllLevels(
  levels: Iterable<java.JavaLevel | null>,
  assets: JavaAssets,
): Iterable<Cr<string>> {
  for (const level of levels) {
    yield level === null ? crOf(SEPARATOR_LEVEL) : compileLevel(level, assets);
  }
}

const SEPARATOR_LEVEL: string = emitMtbon({
  $specialSlots: [],
  $scoreSlots: [],
  $skinBg: 1,
  $skinTiles: 1,
  $badList: [],
  $script: "",
  // $map: ...,
  $playerX: 0,
  $playerY: 0,
});
