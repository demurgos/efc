import { getFieldIndex, HF_LEVEL_HEIGHT, HF_LEVEL_WIDTH, JavaLevel } from "../java/level.mjs";

export type Grid2D<T> = ReadonlyMap<number, ReadonlyMap<number, T>>;

export interface GridPos {
  readonly x: number;
  readonly y: number;
}

export function getInGrid<T>(grid: Grid2D<T>, pos: GridPos): T | undefined {
  const col: ReadonlyMap<number, T> | undefined = grid.get(pos.x);
  return col === undefined ? undefined : col.get(pos.y);
}

interface LevelVortex {
  readonly start: number;
  readonly end: number;
  readonly port: number;
}

export class GridLevelInfo {
  public readonly pos: GridPos;
  public readonly level: JavaLevel;

  private _vorticesLeft: LevelVortex[] | undefined;
  private _vorticesRight: LevelVortex[] | undefined;

  constructor(pos: GridPos, level: JavaLevel) {
    this.pos = pos;
    this.level = level;
  }

  public get vorticesLeft(): ReadonlyArray<LevelVortex> {
    if (this._vorticesLeft === undefined) {
      this.computeVortices();
    }
    return this._vorticesLeft!;
  }

  public get vorticesRight(): ReadonlyArray<LevelVortex> {
    if (this._vorticesRight === undefined) {
      this.computeVortices();
    }
    return this._vorticesRight!;
  }

  private computeVortices(): void {
    this._vorticesLeft = [];
    this._vorticesRight = [];

    const map: ReadonlyArray<ReadonlyArray<string>> = this.level.map;
    const vortexIdx: number = this.level.fields.indexOf("vortex");
    if (vortexIdx === undefined) {
      return;
    }

    function isVortex(x: number, y: number) {
      if (x >= HF_LEVEL_WIDTH || y >= HF_LEVEL_HEIGHT) {
        return false;
      }
      return getFieldIndex(map[x][y]) === vortexIdx;
    }

    // Determine the list of vertical vortices on the side of the level,
    // using Hammerfest's algorithm to determine the port number.
    let port: number = 0;
    // For each x, the highest y with a vortex
    const fieldMap: number[] = Array(HF_LEVEL_WIDTH).fill(-1);

    for (let j: number = 0; j < HF_LEVEL_HEIGHT; j++) {
      for (let i: number = 0; i < HF_LEVEL_WIDTH; i++) {
        if (fieldMap[i] >= j || !isVortex(i, j)) {
          continue;
        }
        fieldMap[i] = j;

        if (isVortex(i + 1, j)) { // horizontal
          let ii: number = i + 1;
          do {
            fieldMap[ii++] = j;
          } while (isVortex(ii, j));

        } else { // vertical
          let jj: number = j + 1;
          while (isVortex(i, jj)) {
            fieldMap[i] = jj++;
          }

          if (i === 0) {
            this._vorticesLeft.push({start: j, end: jj, port});
          } else if (i === HF_LEVEL_WIDTH - 1) {
            this._vorticesRight.push({start: j, end: jj, port});
          }
        }
        port++;
      }
    }
  }
}

export interface GridLink {
  readonly from: GridPos;
  readonly fromPort: number;
  readonly to: GridPos;
  readonly toPort: number;
}

export interface GridChunkInfo {
  readonly levels: ReadonlyArray<GridLevelInfo | null>;
  readonly indices: Grid2D<number>;
  readonly links: ReadonlyArray<GridLink>;
}

export function computeGridInfo(grid: Grid2D<JavaLevel>): GridChunkInfo {
  const levels: (GridLevelInfo | null)[] = [];
  const indices: Map<number, Map<number, number>> = new Map();
  const links: GridLink[] = [];

  // Sort grid columns, with col #0 first
  const cols: [number, ReadonlyMap<number, JavaLevel>][] = [...grid.entries()];
  cols.sort((a, b) => {
    const i: number = a[0] === 0 ? -Infinity : a[0];
    const j: number = b[0] === 0 ? -Infinity : b[0];
    return i === j ? 0 : i - j;
  });

  // Compute levels indices
  for (const [x, col] of cols) {
    if (col.size === 0) {
      continue;
    }

    const colIndices: Map<number, number> = new Map();
    const list: [number, JavaLevel][] = [...col.entries()];
    list.sort((a, b) => a[0] - b[0]); // Sort levels inside columns

    for (const [y, level] of col) {
      colIndices.set(y, levels.length);
      levels.push(new GridLevelInfo({x, y}, level));
    }

    indices.set(x, colIndices);
    levels.push(null);
  }

  // Compute links
  for (const level of levels) {
    if (level === null) {
      continue;
    }
    const leftIndice: number | undefined = getInGrid(indices, {x: level.pos.x - 1, y: level.pos.y});
    if (leftIndice === undefined) {
      continue;
    }
    const leftLevel: GridLevelInfo = levels[leftIndice]!;

    for (const left of leftLevel.vorticesRight) {
      for (const right of level.vorticesLeft) {
        if (left.start === right.start && left.end === right.end) {
          links.push({
            from: leftLevel.pos,
            fromPort: left.port,
            to: level.pos,
            toPort: right.port,
          });
        }
      }
    }
  }

  return {levels, indices, links};
}
