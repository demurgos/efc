import { Vector2D } from "./vector-2d.mjs";

export interface Level {
  readonly specialSlots: ReadonlyArray<Vector2D>;
  readonly scoreSlots: ReadonlyArray<Vector2D>;
  readonly skinBg: number;
  readonly skinTiles: number;
  readonly badList: ReadonlyArray<BadSpawn>;
  readonly script: string;
  /**
   * map[x][y]
   */
  readonly map: ReadonlyArray<ReadonlyArray<number>>;
  readonly playerX: number;
  readonly playerY: number;
}

export interface BadSpawn {
  readonly x: number;
  readonly y: number;
  readonly id: number;
}

export interface EscapedLevel {
  readonly $specialSlots: ReadonlyArray<EscapedVector2D>;
  readonly $scoreSlots: ReadonlyArray<EscapedVector2D>;
  readonly $skinBg: number;
  readonly $skinTiles: number;
  readonly $badList: ReadonlyArray<EscapedBadSpawn>;
  readonly $script: string;
  /**
   * $map[x][y]
   */
  readonly $map: ReadonlyArray<ReadonlyArray<number>>;
  readonly $playerX: number;
  readonly $playerY: number;
}

export interface EscapedVector2D {
  readonly $x: number;
  readonly $y: number;
}

export interface EscapedBadSpawn {
  readonly $x: number;
  readonly $y: number;
  readonly $id: number;
}

export function escapeLevel(lvl: Level): EscapedLevel {
  return {
    $specialSlots: lvl.specialSlots.map(({x, y}) => ({$x: x, $y: y})),
    $scoreSlots: lvl.scoreSlots.map(({x, y}) => ({$x: x, $y: y})),
    $skinBg: lvl.skinBg,
    $skinTiles: lvl.skinTiles,
    $badList: lvl.badList.map(({x, y, id}) => ({$x: x, $y: y, $id: id})),
    $script: lvl.script,
    $map: lvl.map,
    $playerX: lvl.playerX,
    $playerY: lvl.playerY,
  };
}

export function unescapeLevel(lvl: EscapedLevel): Level {
  return {
    specialSlots: lvl.$specialSlots.map(({$x, $y}) => ({x: $x, y: $y})),
    scoreSlots: lvl.$scoreSlots.map(({$x, $y}) => ({x: $x, y: $y})),
    skinBg: lvl.$skinBg,
    skinTiles: lvl.$skinTiles,
    badList: lvl.$badList.map(({$x, $y, $id}) => ({x: $x, y: $y, id: $id})),
    script: lvl.$script,
    map: lvl.$map,
    playerX: lvl.$playerX,
    playerY: lvl.$playerY,
  };
}
