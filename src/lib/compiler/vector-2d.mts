export interface Vector2D {
  readonly x: number;
  readonly y: number;
}
