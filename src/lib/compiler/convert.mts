import {
  CompilerResult as Cr,
  error as crError,
  errors as crErrors,
  join as crJoin,
  joinArray as crJoinArray,
  of as crOf,
} from "../compiler-result.mjs";
import { AnyError, backgroundNotFound, missingBackgroundValue } from "../diagnostic/errors.mjs";
import { Diagnostic } from "../diagnostic/report.mjs";
import { Background, Bad, Field, JavaAssets, Tile } from "../java/assets.mjs";
import * as java from "../java/level.mjs";
import * as hf from "./hf.mjs";
import { Vector2D } from "./vector-2d.mjs";

export function javaToHfLevel(javaLevel: java.JavaLevel, assets: JavaAssets): Cr<hf.Level> {
  const specialSlots: Cr<Vector2D[]> = crOf([...javaLevel.specialSlots]);
  const scoreSlots: Cr<Vector2D[]> = crOf([...javaLevel.scoreSlots]);

  let script: Cr<any>; // string | ParsedBetterScript
  switch (javaLevel.script.type) {
    case "better":
      script = crOf(javaLevel.script.parsed);
      break;
    case "xml":
      script = crOf(javaLevel.script.text);
      break;
    default:
      return javaLevel.script;
  }

  const skinHorizontalTiles: Cr<number> = convertSkinTileId(javaLevel.skins.horizontalTiles, assets.tiles);
  const skinVerticalTiles: Cr<number> = convertSkinTileId(javaLevel.skins.verticalTiles, assets.tiles);

  const badListCr: Cr<hf.BadSpawn>[] = [];
  for (const javaBad of javaLevel.bads) {
    badListCr.push(
      convertBadId(javaBad.id, assets.bads)
        .map(id => ({ id, x: javaBad.x, y: javaBad.y })),
    );
  }
  const badList: Cr<hf.BadSpawn[]> = crJoinArray(badListCr);

  const mapDiags: Diagnostic[] = [];
  const mapRaw: number[][] = [];
  for (const javaColumn of javaLevel.map) {
    const column: number[] = [];
    for (const javaCell of javaColumn) {
      switch (javaCell) {
        case "0":
        case "1":
          column.push(parseInt(javaCell, 10));
          break;
        default: {
          const field: Cr<number> = convertFieldId(javaCell, assets.fields, javaLevel.fields);
          column.push(field.unwrapOr(0));
          for (const diag of field.diagnostics) {
            mapDiags.push(diag);
          }
          break;
        }
      }
    }
    mapRaw.push(column);
  }
  const map: Cr<number[][]> = mapDiags.length === 0 ? crOf(mapRaw) : crErrors(mapDiags);

  const skinBg: Cr<number> = convertSkinBgId(javaLevel.skins.background, assets.backgrounds);

  return crJoin({
    specialSlots,
    scoreSlots,
    skinBg,
    skinTiles: crJoinArray([skinHorizontalTiles, skinVerticalTiles]).map(skins => {
      if (skins[0] !== skins[1]) {
        return skins[0] + 100 * skins[1];
      }
      return skins[0];
    }),
    badList,
    script,
    map,
    playerX: crOf(javaLevel.playerSlot.x),
    playerY: crOf(javaLevel.playerSlot.y),
  });
}

function convertSkinBgId(
  javaBgId: string,
  javaBgAssets: ReadonlyMap<string, Background>,
): Cr<number> {
  const javaBg: Background | undefined = javaBgAssets.get(javaBgId);
  if (javaBg === undefined) {
    return crError(backgroundNotFound(javaBgId));
  } else if (javaBg.value === undefined) {
    return crError(missingBackgroundValue(javaBg));
  } else {
    return crOf(javaBg.value);
  }
}

function convertSkinTileId(javaTileId: string, javaTileAssets: ReadonlyMap<string, Tile>): Cr<number> {
  const javaTile: Tile | undefined = javaTileAssets.get(javaTileId);
  if (javaTile === undefined || javaTile.value === undefined) {
    return crError(new AnyError(`TileNotFound: ${javaTileId}`));
  }
  // TODO: Assert javaTile.id === javaTileId
  return crOf(javaTile.value);
}

function convertBadId(javaBadId: string, javaBadAssets: ReadonlyMap<string, Bad>): Cr<number> {
  const javaBad: Bad | undefined = javaBadAssets.get(javaBadId);
  if (javaBad === undefined || javaBad.value === undefined) {
    return crError(new AnyError(`BadNotFound: ${javaBadId}`));
  }
  // TODO: Assert javaBad.id === javaBadId
  return crOf(javaBad.value);
}

function convertFieldId(
  javaFieldId: string,
  javaFieldAssets: ReadonlyMap<string, Field>,
  fieldMap: ReadonlyArray<string>,
): Cr<number> {
  const fieldIndex: number | undefined = java.getFieldIndex(javaFieldId);
  if (fieldIndex === undefined) {
    return crError(new AnyError(`InvalidFieldId: ${javaFieldId}`));
  }
  const fieldId: string | undefined = fieldMap[fieldIndex];
  if (fieldId === undefined) {
    return crError(new AnyError(`FieldNotFound: javaFieldId = ${javaFieldId}`));
  }
  const field: Field | undefined = javaFieldAssets.get(fieldId);
  if (field === undefined || field.value === undefined) {
    return crError(new AnyError(`FieldNotFound: fieldId = ${fieldId}`));
  }
  return crOf(-field.value);
}
