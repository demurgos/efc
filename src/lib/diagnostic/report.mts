import { Buffer } from "buffer";
import stream from "stream";

export type DiagnosticReporter = (diagnostics: ReadonlyArray<Diagnostic>) => DiagnosticReportSummary;

export type DiagnosticFormatter = (diagnostic: Diagnostic) => string;

export interface DiagnosticReportSummary {
  total: number;
  byCategory: Map<DiagnosticCategory, number>;
}

export interface Diagnostic {
  readonly code: number;
  readonly message: string;
  readonly category: DiagnosticCategory;
  readonly fileUri?: string;
  readonly sourceText?: string;
  readonly startOffset?: number;
  readonly length?: number;
}

export enum DiagnosticCategory {
  /**
   * Non-fatal issue.
   */
  Warning = "warning",

  /**
   * A fatal error happened: no (or invalid) output.
   */
  Error = "error",

  /**
   * Simple info message.
   */
  Message = "message",
}

export function createTtyReporter(ttyStream: stream.Writable): DiagnosticReporter {
  const reporter: TtyReporter = new TtyReporter(ttyStream);
  return (diagnostics: ReadonlyArray<Diagnostic>) => reporter.report(diagnostics);
}

class TtyReporter {
  readonly ttyStream: stream.Writable;

  constructor(ttyStream: stream.Writable) {
    this.ttyStream = ttyStream;
  }

  report(diagnostics: ReadonlyArray<Diagnostic>): DiagnosticReportSummary {

    let total: number = 0;
    const byCategory: Map<DiagnosticCategory, number> = new Map(Object.values(DiagnosticCategory).map(key => [key, 0]));

    for (const diagnostic of diagnostics) {
      const cat: DiagnosticCategory = diagnostic.category;
      total++;
      byCategory.set(cat, byCategory.get(cat)! + 1);
      this.ttyStream.write(Buffer.from(formatOneLine(diagnostic)));
    }

    return { total, byCategory };
  }
}

function formatOneLine(diagnostic: Diagnostic): string {
  const chunks: string[] = [];

  switch (diagnostic.category) {
    case DiagnosticCategory.Error:
      chunks.push("[ERR ");
      break;
    case DiagnosticCategory.Warning:
      chunks.push("[WARN ");
      break;
    case DiagnosticCategory.Message:
      chunks.push("[MSG ");
      break;
    default:
      return diagnostic.category;
  }

  chunks.push(diagnostic.code.toString().padStart(4, "0"));
  chunks.push("] ");

  if (diagnostic.startOffset !== undefined) {
    chunks.push(diagnostic.fileUri === undefined ? "<unknown>" : diagnostic.fileUri);

    if (diagnostic.sourceText === undefined) {
      // No source text, so show raw character indices.
      chunks.push(" chars ");
      chunks.push(diagnostic.startOffset.toString());
      if (diagnostic.length !== undefined) {
        chunks.push("-");
        chunks.push((diagnostic.startOffset + diagnostic.length).toString());
      }
    } else {
      // We have a source text, so convert character indices to text positions.
      chunks.push(":");
      chunks.push(charRangeToPrettyTextPos(
        diagnostic.sourceText,
        diagnostic.startOffset,
        diagnostic.length,
      ));
    }

    chunks.push(": ");

  } else if (diagnostic.fileUri !== undefined) {
    chunks.push(diagnostic.fileUri);
    chunks.push(": ");
  }

  chunks.push(diagnostic.message);
  chunks.push("\n");
  return chunks.join("");
}

interface TextPos {
  line: number;
  col: number;
}

function charRangeToPrettyTextPos(text: string, offset: number, length?: number): string {
  const pos: TextPos[] = length === undefined ?
    charLocationsToTextPos(text, offset) :
    charLocationsToTextPos(text, offset, offset + length);

  const prefix: string = `${pos[0].line}:${pos[0].col}`;

  let postfix: string = "";
  if (pos[0].line === pos[1].line) {
    if (pos[0].col !== pos[1].col) {
      postfix = `-${pos[1].col}`;
    }
  } else {
    postfix = `-${pos[1].line}:${pos[1].col}`;
  }
  return prefix + postfix;
}

function charLocationsToTextPos(text: string, ...locs: number[]): TextPos[] {
  const result: TextPos[] = [];
  locs.sort((a, b) => a - b);

  let curIdx: number = 0;
  let line: number = 0;
  let endPrevLine: number = Math.min(-1, locs[0]);
  let endCurLine: number = -1;

  while (curIdx < locs.length) {
    const curPos: number = locs[curIdx];

    if (curPos <= endCurLine) {
      const col: number = curPos - endPrevLine;
      result.push({ line, col });
      curIdx++;
      continue;
    }

    line++;
    endPrevLine = endCurLine;
    endCurLine = text.indexOf("\n", endPrevLine + 1);
    if (endCurLine < 0) {
      endCurLine = Infinity;
    }
  }

  return result;
}
