import { ScriptError } from "@eternalfest/better-scripts/lib/error.js";

import { Background } from "../java/assets.mjs";
import {
  ANY_ERROR,
  ANY_WARNING,
  BACKGROUND_NOT_FOUND,
  BETTER_SCRIPT_ERROR,
  FS_IO_ERROR,
  MISSING_BACKGROUND_VALUE,
  UNKNOWN_SCRIPT_TYPE_ERROR,
  XML_CHILD_NON_UNIQUE,
  XML_CHILD_NOT_FOUND,
  XML_MISSING_ATTRIBUTE,
} from "./codes.mjs";
import { Diagnostic, DiagnosticCategory } from "./report.mjs";
import Cheerio = cheerio.Cheerio;

export class UnknownScriptTypeError implements Diagnostic {
  code: typeof UNKNOWN_SCRIPT_TYPE_ERROR;
  category: DiagnosticCategory.Error;
  message: string;
  fileUri?: string;
  sourceText?: string;
  startOffset?: number;
  length?: number;

  scriptType: string;

  constructor(scriptType: string) {
    this.code = UNKNOWN_SCRIPT_TYPE_ERROR;
    this.category = DiagnosticCategory.Error;
    this.message = `Unexpected script type: ${scriptType}`;
    this.fileUri = undefined;
    this.sourceText = undefined;
    this.startOffset = undefined;
    this.length = undefined;

    this.scriptType = scriptType;
  }
}

export class AnyError implements Diagnostic {
  code: typeof ANY_ERROR;
  category: DiagnosticCategory.Error;
  message: string;
  fileUri?: string;
  sourceText?: string;
  startOffset?: number;
  length?: number;

  constructor(message: string) {
    this.code = ANY_ERROR;
    this.category = DiagnosticCategory.Error;
    this.message = message;
    this.fileUri = undefined;
    this.sourceText = undefined;
    this.startOffset = undefined;
    this.length = undefined;
  }
}

export class AnyWarning implements Diagnostic {
  message: string;
  code: typeof ANY_WARNING;
  category: DiagnosticCategory.Warning;
  fileUri?: string;
  sourceText?: string;
  startOffset?: number;
  length?: number;

  constructor(message: string) {
    this.message = message;
    this.code = ANY_WARNING;
    this.category = DiagnosticCategory.Warning;
    this.fileUri = undefined;
    this.sourceText = undefined;
    this.startOffset = undefined;
    this.length = undefined;
  }
}

export class BetterScriptError implements Diagnostic {
  code: typeof BETTER_SCRIPT_ERROR;
  category: DiagnosticCategory.Error;
  message: string;
  fileUri?: string;
  sourceText?: string;
  startOffset?: number;
  length?: number;

  constructor(scriptError: ScriptError) {
    this.code = BETTER_SCRIPT_ERROR;
    this.category = DiagnosticCategory.Error;
    this.message = scriptError.msg;
    this.fileUri = undefined;
    this.sourceText = undefined;
    this.startOffset = undefined;
    this.length = undefined;
  }
}

export class FsIoError implements Diagnostic {
  code: typeof FS_IO_ERROR;
  category: DiagnosticCategory.Error;
  message: string;
  fileUri?: string;
  sourceText?: string;
  startOffset?: number;
  length?: number;

  cause: NodeJS.ErrnoException;

  constructor(cause: NodeJS.ErrnoException) {
    this.code = FS_IO_ERROR;
    this.category = DiagnosticCategory.Error;
    this.message = `FS IO error: ${cause.message}`;
    this.fileUri = undefined;
    this.sourceText = undefined;
    this.startOffset = undefined;
    this.length = undefined;

    this.cause = cause;
  }
}

interface Offsets {
  readonly startOffset?: number;
  readonly length?: number;
}

interface ElementIndices {
  readonly startIndex?: number;
  readonly endIndex?: number;
}

function getElementOffsets(node: Cheerio): Offsets {
  const elem: cheerio.Element | undefined = node[0];
  const startIndex: number | undefined = elem !== undefined ? (elem as ElementIndices).startIndex : undefined;
  const endIndex: number | undefined = elem !== undefined ? (elem as ElementIndices).endIndex : undefined;
  if (startIndex !== undefined) {
    if (endIndex !== undefined) {
      return {
        startOffset: Math.min(startIndex, endIndex),
        length: Math.abs(startIndex - endIndex),
      };
    } else {
      return {startOffset: startIndex};
    }
  } else {
    return endIndex !== undefined ? {startOffset: endIndex} : {};
  }
}

export interface XmlChildNotFound extends Diagnostic {
  readonly code: typeof XML_CHILD_NOT_FOUND;
  readonly selector: string;
}

export function xmlChildNotFound(
  parent: Cheerio,
  selector: string,
  category: DiagnosticCategory = DiagnosticCategory.Error,
): XmlChildNotFound {
  const message: string = `XML child not found for selector ${JSON.stringify(selector)}`;
  const {startOffset, length} = getElementOffsets(parent);
  return {
    code: XML_CHILD_NOT_FOUND,
    category,
    message,
    fileUri: undefined,
    sourceText: undefined,
    startOffset,
    length,
    selector,
  };
}

export interface XmlChildNonUnique extends Diagnostic {
  readonly code: typeof XML_CHILD_NON_UNIQUE;
  readonly category: DiagnosticCategory.Error;
  readonly selector: string;
}

export function xmlChildNonUnique(parent: Cheerio, selector: string): XmlChildNonUnique {
  const message: string = `XML child must be unique for selector ${JSON.stringify(selector)}`;
  const {startOffset, length} = getElementOffsets(parent);
  return {
    code: XML_CHILD_NON_UNIQUE,
    category: DiagnosticCategory.Error,
    message,
    fileUri: undefined,
    sourceText: undefined,
    startOffset,
    length,
    selector,
  };
}

export interface XmlMissingAttribute extends Diagnostic {
  readonly code: typeof XML_MISSING_ATTRIBUTE;
  readonly category: DiagnosticCategory.Error;
  readonly attribute: string;
}

export function xmlMissingAttribute(node: Cheerio, attribute: string): XmlMissingAttribute {
  const message: string = `Missing XML attribute ${JSON.stringify(attribute)}`;
  const {startOffset, length} = getElementOffsets(node);
  return {
    code: XML_MISSING_ATTRIBUTE,
    category: DiagnosticCategory.Error,
    message,
    fileUri: undefined,
    sourceText: undefined,
    startOffset,
    length,
    attribute,
  };
}

export interface BackgroundNotFound extends Diagnostic {
  readonly code: typeof BACKGROUND_NOT_FOUND;
  readonly category: DiagnosticCategory.Error;
  readonly key: string;
}

export function backgroundNotFound(key: string): BackgroundNotFound {
  const message: string = `BackgroundNotFound: ${JSON.stringify(key)}`;
  return {
    code: BACKGROUND_NOT_FOUND,
    category: DiagnosticCategory.Error,
    message,
    fileUri: undefined,
    sourceText: undefined,
    startOffset: undefined,
    length: undefined,
    key,
  };
}

export interface MissingBackgroundValue extends Diagnostic {
  readonly code: typeof MISSING_BACKGROUND_VALUE;
  readonly category: DiagnosticCategory.Error;
  readonly background: Background;
}

export function missingBackgroundValue(background: Background): MissingBackgroundValue {
  const message: string = `MissingBackgroundValue: ${JSON.stringify(background)}`;
  return {
    code: MISSING_BACKGROUND_VALUE,
    category: DiagnosticCategory.Error,
    message,
    fileUri: undefined,
    sourceText: undefined,
    startOffset: undefined,
    length: undefined,
    background,
  };
}
