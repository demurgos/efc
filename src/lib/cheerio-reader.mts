import { CompilerResult as Cr, error as crError, of as crOf } from "./compiler-result.mjs";
import { xmlChildNonUnique, xmlChildNotFound, xmlMissingAttribute } from "./diagnostic/errors.mjs";
type Cheerio = cheerio.Cheerio;

export function readOneChild(node: Cheerio, selector: string): Cr<Cheerio> {
  const children: Cheerio = node.children(selector);
  if (children.length === 1) {
    return crOf(children);
  }
  return crError(children.length === 0 ? xmlChildNotFound(node, selector) : xmlChildNonUnique(node, selector));
}

export function readOneOrNoneChild(node: Cheerio, selector: string): Cr<Cheerio | undefined> {
  const children: Cheerio = node.children(selector);
  if (children.length <= 1) {
    return crOf(children.length === 0 ? undefined : children);
  }
  return crError(xmlChildNonUnique(node, selector));
}

export function cheerioMapChildren<R>(
  $: cheerio.Root,
  node: Cheerio,
  selector: string,
  fn: (child: Cheerio) => R,
): R[] {
  const result: R[] = [];
  node
    .children(selector)
    .each((_: number, element: cheerio.Element): void => {
      result.push(fn($(element)));
    });
  return result;
}

export function readAttr(node: Cheerio, attrName: string): Cr<string> {
  const value: string | undefined = node.attr(attrName);
  if (value !== undefined) {
    return crOf(value);
  }
  return crError(xmlMissingAttribute(node, attrName));
}
