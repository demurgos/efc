import fs from "fs";
import * as rx from "rxjs";
import { switchMap as rxSwitchMap } from "rxjs/operators";
import { URL } from "url";

import { CompilerResult as Cr, error as crError, of as crOf } from "./compiler-result.mjs";
import { FsIoError } from "./diagnostic/errors.mjs";
import { getObservableFileVersion } from "./observable-fs.mjs";

export function observeTextFile(fileUri: URL): rx.Observable<Cr<string>> {
  return getObservableFileVersion(fileUri)
    .pipe(rxSwitchMap(() => rx.from(readTextFile(fileUri))));
}

export async function readTextFile(filePath: fs.PathLike): Promise<Cr<string>> {
  try {
    const text: string = await readTextFileAsync(filePath);
    return crOf(text);
  } catch (err) {
    return crError(new FsIoError(err as NodeJS.ErrnoException));
  }
}

async function readTextFileAsync(filePath: fs.PathLike): Promise<string> {
  return new Promise<string>((resolve, reject): void => {
    fs.readFile(
      filePath,
      {encoding: "utf-8"},
      (err: NodeJS.ErrnoException | null, text: string): void => {
        if (err !== null) {
          reject(err);
        } else {
          resolve(text);
        }
      },
    );
  });
}

const CHAR_UPPER_A: number = 0x41;
const CHAR_UPPER_Z: number = 0x5A;
const CHAR_LOWER_A: number = 0x61;
const CHAR_LOWER_Z: number = 0x7A;
const CHAR_UNDERSCORE: number = 0x5F;
const CHAR_0: number = 0x30;
const CHAR_9: number = 0x39;

export function isAsciiIdentifierStart(code: number): boolean {
  return (code >= CHAR_UPPER_A && code <= CHAR_UPPER_Z)
    || (code >= CHAR_LOWER_A && code <= CHAR_LOWER_Z)
    || code === CHAR_UNDERSCORE;
}

export function isAsciiIdentifierContinue(code: number): boolean {
  return isAsciiIdentifierStart(code) || (code >= CHAR_0 && code <= CHAR_9);
}

/**
 * Mark a block of code as unreachable.
 *
 * Reaching it will throw an error reporting the broken assertion.
 *
 * The witness value is used to exhibit the broken typesystem invariant
 * (useful in switch/case statements).
 */
export function unreachable(_witness: never, message?: string, ): never {
  if (typeof  message === "string") {
    throw new Error(`Entered unreachable code: ${message}`);
  } else {
    throw new Error("Entered unreachable code");
  }
}
