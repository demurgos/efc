import { LandsLocale } from "./lands-locale.mjs";
import Cheerio = cheerio.Cheerio;

export interface Locale {
  readonly statics: Cheerio;
  readonly items: Cheerio;
  readonly lands: LandsLocale;
  readonly keys: Cheerio;
  readonly quests: undefined;
  readonly families: undefined;
}
