import { parse as bsParse } from "@eternalfest/better-scripts";
import { ScriptParseResult } from "@eternalfest/better-scripts/lib/parse.js";
import cheerio from "cheerio";

import { cheerioMapChildren, readAttr, readOneChild, readOneOrNoneChild } from "../cheerio-reader.mjs";
import { Vector2D } from "../compiler/vector-2d.mjs";
import {
  CompilerResult as Cr,
  error as crError,
  errors as crErrors,
  join as crJoin,
  joinArray as crJoinArray,
  of as crOf,
  warn as crWarn,
} from "../compiler-result.mjs";
import { AnyError, BetterScriptError, UnknownScriptTypeError, xmlChildNotFound } from "../diagnostic/errors.mjs";
import { DiagnosticCategory } from "../diagnostic/report.mjs";
import Cheerio = cheerio.Cheerio;

export interface JavaLevel {
  readonly url?: string;
  readonly version: string;
  readonly uid?: string;
  readonly name: string;
  readonly description: string;
  readonly script: Script;
  readonly skins: Skins;
  readonly fields: ReadonlyArray<string>;
  // map[x][y]
  readonly map: ReadonlyArray<ReadonlyArray<string>>;
  readonly playerSlot: Vector2D;
  readonly specialSlots: ReadonlyArray<Vector2D>;
  readonly scoreSlots: ReadonlyArray<Vector2D>;
  readonly bads: ReadonlyArray<Bad>;
}

export type Script = XmlScript | BetterScript;

export interface XmlScript {
  readonly type: "xml";
  readonly text: string;
}

export interface BetterScript {
  readonly type: "better";
  readonly parsed: any;
}

export interface Skins {
  readonly background: string;
  readonly horizontalTiles: string;
  readonly verticalTiles: string;
}

export interface Bad {
  readonly id: string;
  readonly x: number;
  readonly y: number;
}

interface Sprite {
  readonly id: string;
  readonly subId: string;
  readonly x: number;
  readonly y: number;
  readonly mirrorX: boolean;
  readonly mirrorY: boolean;
  readonly rotation: number;
}

export const HF_LEVEL_WIDTH: number = 20;
export const HF_LEVEL_HEIGHT: number = 25;
const DEFAULT_PLAYER_SLOT: Vector2D = {x: 0, y: 0};

export function parseString(lvlText: string): Cr<JavaLevel> {
  const $: cheerio.Root = cheerio.load(
    lvlText,
    {xmlMode: true, withStartIndices: true, withEndIndices: true},
  );
  return readLevel($, $("level")).mapDiagnostics(diag => ({...diag, sourceText: lvlText}));
}

const UPPER_CASE_A: number = "A".codePointAt(0)!;
export function getFieldIndex(fieldChar: string): number | undefined {
  const cp: number = fieldChar.codePointAt(0)!;
  return cp < UPPER_CASE_A ? undefined : cp - UPPER_CASE_A;
}

function readLevel($: cheerio.Root, node: Cheerio): Cr<JavaLevel> {
  return crJoin({
    version: readAttr(node, "version"),
    uid: crOf(node.attr("uid")),
    name: readOneChild(node, "name").map(node => node.text()),
    description: readOneChild(node, "name").map(node => node.text()),
    script: readOneOrNoneChild(node, "script")
      .flatMap(child => child !== undefined ? readScript(child) : crOf(getDefaultScript())),
    skins: readOneChild(node, "skins").flatMap(readSkins),
    fields: readOneOrNoneChild(node, "rayons")
      .flatMap(child => child !== undefined ? readFields($, child) : crOf(getDefaultFields())),
    map: readOneChild(node, "plateformes").flatMap(readTileMap),
    playerSlot: readOneOrNoneChild(node, "player")
      .flatMap(child => child !== undefined
        ? readVector2D(child)
        : crWarn(DEFAULT_PLAYER_SLOT, xmlChildNotFound(node, "player", DiagnosticCategory.Warning)),
      ),
    specialSlots: crJoinArray(cheerioMapChildren($, node, "effet", readVector2D)),
    scoreSlots: crJoinArray(cheerioMapChildren($, node, "points", readVector2D)),
    bads: readOneOrNoneChild(node, "badlist")
      .flatMap(child => child !== undefined ? readBadList($, child) : crOf([])),
  });
}

function readScript(node: Cheerio): Cr<Script> {
  const type: string | undefined = node.attr("type");
  switch (type) {
    case "new": {
      const text: string = node.contents().text();
      const parseResult: ScriptParseResult = bsParse(text);
      if (parseResult.errors.length > 0) {
        const diagnostics: BetterScriptError[] = [];
        for (const err of parseResult.errors) {
          diagnostics.push(new BetterScriptError(err));
        }
        return crErrors(diagnostics);
      } else {
        const parsed: any = parseResult.script.export();
        const script: BetterScript = {type: "better", parsed};
        return crOf(script);
      }
    }
    case undefined:
    case "xml": {
      // We must explicitly get the child text nodes because `cheerio` ignore
      // text in `script` tags.
      const text: string = node.contents().text();
      const script: XmlScript = {type: "xml", text};
      return crOf(script);
    }
    default: {
      if (node.contents().length === 0) {
        const script: XmlScript = getDefaultScript();
        return crOf(script);
      } else {
        return crError(new UnknownScriptTypeError(type));
      }
    }
  }
}

function getDefaultScript(): XmlScript {
  return {type: "xml", text: ""};
}

function readSkins(node: Cheerio): Cr<Skins> {
  return crJoin({
    background: readOneChild(node, "fond").flatMap(node => readAttr(node, "id")),
    horizontalTiles: readOneChild(node, "plateformeH").flatMap(node => readAttr(node, "id")),
    verticalTiles: readOneChild(node, "plateformeV").flatMap(node => readAttr(node, "id")),
  });
}

function readFields($: cheerio.Root, node: Cheerio): Cr<string[]> {
  return crJoinArray(cheerioMapChildren($, node, "r", child => readAttr(child, "name")))
    .map(fields => fields[0] === "none" ? fields.slice(1) : fields);
}

function getDefaultFields(): string[] {
  return [
    "blanc",
    "noir",
    "bleu",
    "vert",
    "rouge",
    "teleport",
    "vortex",
    "soccer_violet",
    "soccer_jaune",
    "J-O",
    "nobombs",
  ];
}

function readTileMap(node: Cheerio): Cr<string[][]> {
  const HF_LEVEL_CELL_COUNT: number = HF_LEVEL_WIDTH * HF_LEVEL_HEIGHT;
  const str: string = node.text().trim();
  if (str.length !== HF_LEVEL_CELL_COUNT) {
    return crError(new AnyError(`Invalid map length, actual: ${str.length}, expected: ${HF_LEVEL_CELL_COUNT}`));
  }
  const map: string[][] = [];
  let nextColumn: string[] = [];
  for (const chr of str) {
    nextColumn.push(chr);
    if (nextColumn.length === HF_LEVEL_HEIGHT) {
      map.push(nextColumn);
      nextColumn = [];
    }
  }
  return crOf(map);
}

function readVector2D(node: Cheerio): Cr<Vector2D> {
  return crJoin({
    x: readAttr(node, "x").map(parseFloat).flatMap(checkFloat),
    y: readAttr(node, "y").map(parseFloat).flatMap(checkFloat),
  });

  function checkFloat(value: number): Cr<number> {
    return isNaN(value) ? crError(new AnyError(`Expected valid float, got: ${value}`)) : crOf(value);
  }
}

function readBadList($: cheerio.Root, node: Cheerio): Cr<Bad[]> {
  return crJoinArray(cheerioMapChildren($, node, "bad", readBad));
}

function readBad(node: Cheerio): Cr<Bad> {
  return readSprite(node).map(sprite => ({id: sprite.id, x: sprite.x, y: sprite.y}));
}

function readSprite(node: Cheerio): Cr<Sprite> {
  const id: Cr<string> = readAttr(node, "id");
  const subId: Cr<string> = readAttr(node, "id");
  const x: Cr<number> = readAttr(node, "x").map(parseFloat);
  const y: Cr<number> = readAttr(node, "y").map(parseFloat);
  const flipCr: Cr<number> = readAttr(node, "f").map(str => parseInt(str, 10));
  const mirrorX: Cr<boolean> = flipCr.map(flip => (flip & 0b01) !== 0);
  const mirrorY: Cr<boolean> = flipCr.map(flip => (flip & 0b10) !== 0);
  const rotation: Cr<number> = readAttr(node, "r").map(str => parseInt(str, 10));

  return crJoin({id, subId, x, y, mirrorX, mirrorY, rotation});
}
