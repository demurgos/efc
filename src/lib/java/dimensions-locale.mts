import cheerio from "cheerio";
import { URL } from "url";

import { readAttr } from "../cheerio-reader.mjs";
import { LevelRefPos, parseLevelRefExpr } from "../compiler/links.mjs";
import {
  CompilerResult as Cr,
  joinArray as crJoinArray,
  joinMapEntries as crJoinMapEntries,
} from "../compiler-result.mjs";
import { readTextFile } from "../utils.mjs";
import Cheerio = cheerio.Cheerio;

export type JavaDimensionsLocale = ReadonlyMap<LevelRefPos, string>;

export async function fromPath(configPath: URL): Promise<Cr<JavaDimensionsLocale>> {
  const text: Cr<string> = await readTextFile(configPath);
  return text.flatMap(fromString).mapDiagnostics(d => ({...d, fileUri: configPath}));
}

export function fromString(text: string): Cr<ReadonlyMap<LevelRefPos, string>> {
  const $: cheerio.Root = cheerio.load(text, {xmlMode: true});
  return readDimensionsLocale($, $("dimensions"));
}

function readDimensionsLocale($: cheerio.Root, node: Cheerio): Cr<ReadonlyMap<LevelRefPos, string>> {
  const levels: Cr<[LevelRefPos, string]>[] = [];

  node
    .children("dimension")
    .each((_: number, elem: cheerio.Element): void => {
      const setNode: Cheerio = $(elem);
      readAttr(setNode, "id").map(did => {
        setNode
          .children("level")
          .each((_: number, elem: cheerio.Element): void => {
            levels.push(readLevelLocale($(elem)).map(lvlLocale => {
              const [key, text] = lvlLocale;
              const levelId: number = parseInt(key, 10);
              return [{refType: "pos", did, levelId}, text];
            }));
          });
      });
    });

  node
    .children("level")
    .each((_: number, elem: cheerio.Element): void => {
      levels.push(readLevelLocale($(elem)).flatMap(lvlLocale => {
        const [key, text] = lvlLocale;
        return parseLevelRefExpr(key).map(key => [key, text]);
      }));
    });

  node
    .children("contree")
    .each((_: number, elem: cheerio.Element): void => {
      levels.push(readLevelLocale($(elem)).map(lvlLocale => {
        const [key, text] = lvlLocale;
        return [{refType: "tag", tag: key}, text];
      }));
    });

  return crJoinMapEntries(levels);
}

function readLevelLocale(node: Cheerio): Cr<[string, string]> {
  const key: Cr<string> = readAttr(node, "id");
  const text: Cr<string> = readAttr(node, "name");
  return crJoinArray([key, text]);
}
