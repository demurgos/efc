import cheerio from "cheerio";
import { fromPosixPath, fromSysPath, toSysPath } from "furi";
import path from "path";
import { fileURLToPath,URL } from "url";

import { readAttr, readOneChild } from "../cheerio-reader.mjs";
import {
  CompilerResult as Cr,
  error as crError,
  errors as crErrors,
  join as crJoin,
  joinArray as crJoinArray,
  joinMapEntries as crJoinMapEntries,
  of as crOf,
} from "../compiler-result.mjs";
import { AnyError } from "../diagnostic/errors.mjs";
import { Diagnostic } from "../diagnostic/report.mjs";
import { readTextFile } from "../utils.mjs";
import { EfcAssetsConfig } from "./config.mjs";
import Cheerio = cheerio.Cheerio;

export interface JavaAssets {
  readonly fields: ReadonlyMap<string, Field>;
  readonly backgrounds: ReadonlyMap<string, Background>;
  readonly tiles: ReadonlyMap<string, Tile>;
  readonly bads: ReadonlyMap<string, Bad>;
}

export interface AssetBase {
  readonly id: string;
  readonly name: string;
  readonly value?: number;
}

export interface Field extends AssetBase {
  readonly imageUrl: string;
}

export interface Background extends AssetBase {
  readonly imageUrl: string;
}

export interface Tile extends AssetBase {
  readonly bodyImageUrl: string;
  readonly endImageUrl: string;
}

export interface Bad extends AssetBase {
  readonly images: ReadonlyArray<any>;
}

const BASE_ASSETS_DIR: URL = fromSysPath(path.resolve(fileURLToPath(import.meta.url), "..", "..", "..", "assets"));

export async function getBaseAssets(): Promise<Cr<JavaAssets>> {
  return fromDirectory(BASE_ASSETS_DIR);
}

export function empty(): JavaAssets {
  return {fields: new Map(), backgrounds: new Map(), tiles: new Map(), bads: new Map()};
}

export async function fromConfig(config: EfcAssetsConfig): Promise<Cr<JavaAssets>> {
  const assetsPromises: Promise<Cr<JavaAssets>>[] = [
    config.useBaseAssets ? getBaseAssets() : Promise.resolve(crOf(empty())),
  ];
  for (const extra of config.roots) {
    assetsPromises.push(fromDirectory(extra));
  }
  const assets: Cr<JavaAssets>[] = await Promise.all(assetsPromises);
  return crJoinArray(assets).map(assets => assets.reduce(merge));
}

export async function fromDirectory(dirUrl: URL): Promise<Cr<JavaAssets>> {
  const dir: string = toSysPath(dirUrl);
  const fieldsXmlPath: string = path.posix.join(dir, "xml", "rayons.xml");
  const backgroundsXmlPath: string = path.posix.join(dir, "xml", "fonds.xml");
  const tilesXmlPath: string = path.posix.join(dir, "xml", "plateformes.xml");
  const spritesXmlPath: string = path.posix.join(dir, "xml", "sprites.xml");

  const fieldsXml: Cr<string> = await readTextFile(fieldsXmlPath);
  const backgroundsXml: Cr<string> = await readTextFile(backgroundsXmlPath);
  const tilesXml: Cr<string> = await readTextFile(tilesXmlPath);
  const spritesXml: Cr<string> = await readTextFile(spritesXmlPath);

  return crJoin({
    fields: fieldsXml.flatMap(xml => fieldsFromXmlString(xml, dir))
      .mapDiagnostics(d => ({...d, fileUri: fromPosixPath(fieldsXmlPath).toString()})),
    backgrounds: backgroundsXml.flatMap(xml => backgroundsFromXmlString(xml, dir))
      .mapDiagnostics(d => ({...d, fileUri: fromPosixPath(backgroundsXmlPath).toString()})),
    tiles: tilesXml.flatMap(xml => tilesFromXmlString(xml, dir))
      .mapDiagnostics(d => ({...d, fileUri: fromPosixPath(tilesXmlPath).toString()})),
    bads: spritesXml.flatMap(xml => badsFromXmlString(xml, dir))
      .mapDiagnostics(d => ({...d, fileUri: fromPosixPath(spritesXmlPath).toString()})),
  });
}

export function merge(base: JavaAssets, extra: JavaAssets): JavaAssets {
  return {
    fields: new Map([...base.fields, ...extra.fields]),
    backgrounds: new Map([...base.backgrounds, ...extra.backgrounds]),
    tiles: new Map([...base.tiles, ...extra.tiles]),
    bads: new Map([...base.bads, ...extra.bads]),
  };
}

function fieldsFromXmlString(text: string, rootDir: string): Cr<ReadonlyMap<string, Field>> {
  const $: cheerio.Root = cheerio.load(
    text,
    {xmlMode: true, withStartIndices: true, withEndIndices: true},
  );
  return readOneChild($.root(), "rayons")
    .flatMap((node: Cheerio) => readFields($, node, path.posix.join(rootDir, "rayons")));
}

export function backgroundsFromXmlString(
  text: string,
  rootDir: string,
): Cr<ReadonlyMap<string, Background>> {
  const $: cheerio.Root = cheerio.load(
    text,
    {xmlMode: true, withStartIndices: true, withEndIndices: true},
  );
  return readOneChild($.root(), "fonds")
    .flatMap((node: Cheerio) => readBackgrounds($, node, path.posix.join(rootDir, "fonds")));
}

function tilesFromXmlString(text: string, rootDir: string): Cr<ReadonlyMap<string, Tile>> {
  const $: cheerio.Root = cheerio.load(
    text,
    {xmlMode: true, withStartIndices: true, withEndIndices: true},
  );
  return readOneChild($.root(), "plateformes")
    .flatMap((node: Cheerio) => readTiles($, node, path.posix.join(rootDir, "plateformes")));
}

function badsFromXmlString(text: string, rootDir: string): Cr<ReadonlyMap<string, Bad>> {
  const $: cheerio.Root = cheerio.load(
    text,
    {xmlMode: true, withStartIndices: true, withEndIndices: true},
  );
  return readOneChild($.root(), "sprites")
    .flatMap((node: Cheerio) => readBads($, node, path.posix.join(rootDir, "sprites")));
}

function readFields(
  $: cheerio.Root,
  node: Cheerio,
  imagesDir: string,
): Cr<ReadonlyMap<string, Field>> {
  return assetsFromXmlNode($, node, "type", (base: AssetBase, child: Cheerio) => {
    return readAttr(child, "file").map(imageStr => {
      const {id, name, value} = base;
      const imageUrl: string = path.posix.join(imagesDir, imageStr);
      return {id, name, value, imageUrl};
    });
  });
}

function readBackgrounds(
  $: cheerio.Root,
  node: Cheerio,
  imagesDir: string,
): Cr<ReadonlyMap<string, Background>> {
  return assetsFromXmlNode($, node, "type", (base: AssetBase, child: Cheerio) => {
    return readAttr(child, "file").map(imageStr => {
      const {id, name, value} = base;
      const imageUrl: string = path.posix.join(imagesDir, imageStr);
      return {id, name, value, imageUrl};
    });
  });
}

function readTiles($: cheerio.Root, node: Cheerio, imagesDir: string): Cr<ReadonlyMap<string, Tile>> {
  const bodyImagesDir: string = path.posix.join(imagesDir, "bouts");
  const endImagesDir: string = path.posix.join(imagesDir, "corps");

  return assetsFromXmlNode($, node, "type", (base: AssetBase, child: Cheerio) => {
    return readAttr(child, "file").map(imageStr => {
      const {id, name, value} = base;
      const bodyImageUrl: string = path.posix.join(bodyImagesDir, imageStr);
      const endImageUrl: string = path.posix.join(endImagesDir, imageStr);
      return {id, name, value, bodyImageUrl, endImageUrl};
    });
  });
}

// TODO: Read individual images of sprites
function readBads($: cheerio.Root, node: Cheerio, _imagesDir: string): Cr<ReadonlyMap<string, Bad>> {
  return assetsFromXmlNode($, node, "fruit", (base: AssetBase, _: Cheerio) => {
    const {id, name, value} = base;
    return crOf({id, name, value, images: []});
  });
}

function readAssetBase(node: Cheerio): Cr<AssetBase> {
  const id: Cr<string> = readAttr(node, "ID");
  const name: Cr<string> = readAttr(node, "name");
  const valueStr: string | undefined = node.attr("value");

  const value: Cr<number | undefined> = crOf(valueStr !== undefined ? parseInt(valueStr, 10) : undefined);

  return crJoin({id, name, value});
}

function assetsFromXmlNode<T extends AssetBase>(
  $: cheerio.Root,
  node: Cheerio,
  type: string,
  fromNode: (base: AssetBase, node: Cheerio) => Cr<T>,
): Cr<ReadonlyMap<string, T>> {

  const assetsList: Cr<[string, T]>[] = [];
  const aliasesList: Cr<[string, string]>[] = [];

  node
    .children()
    .each((_: number, _elem: cheerio.Element): void => {
      const elem: cheerio.TagElement = _elem as any;
      const node: Cheerio = $(elem);
      if (elem.name === "alias") {
        const aliasId: Cr<string> = readAttr(node, "ID");
        const aliasRef: Cr<string> = readAttr(node, "ref");
        aliasesList.push(crJoinArray([aliasId, aliasRef]));

      } else if (elem.name === type) {
        assetsList.push(readAssetBase(node)
          .flatMap(base => fromNode(base, node))
          .map(asset => [asset.id, asset]));
      }
    });

  const assets: Cr<Map<string, T>> = crJoinMapEntries(
    assetsList,
    (key, _old, _new) => crError(new AnyError(`DuplicateAssetId: ${key}`)),
  );
  const aliases: Cr<[string, string][]> = crJoinArray(aliasesList);

  return crJoinArray([assets, aliases]).flatMap(vals => {
    const [assets, aliases] = vals;
    const diags: Diagnostic[] = [];
    for (const [alias, ref] of aliases) {
      const refAsset: T | undefined = assets.get(ref);
      if (refAsset === undefined) {
        diags.push(new AnyError(`UnknownAssetId: ${ref}`));
      } else if (assets.has(alias)) {
        diags.push(new AnyError(`DuplicateAssetId: ${alias}`));
      } else {
        assets.set(alias, refAsset);
      }
    }
    return diags.length === 0 ? crOf(assets) : crErrors(diags);
  });
}
