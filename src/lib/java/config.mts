import cheerio from "cheerio";
import { toSysPath } from "furi";
import sysPath from "path";
import * as rx from "rxjs";
import * as rxOp from "rxjs/operators";
import url, { URL } from "url";

import { readAttr } from "../cheerio-reader.mjs";
import {
  CompilerResult as Cr,
  error as crError,
  join as crJoin,
  joinArray as crJoinArray,
  joinMapEntries as crJoinMapEntries,
  of as crOf,
} from "../compiler-result.mjs";
import { AnyError } from "../diagnostic/errors.mjs";
import { observeTextFile } from "../utils.mjs";
import Cheerio = cheerio.Cheerio;

export interface EfcConfig {
  /**
   * Root directory, used to resolve relative URL fragments.
   */
  readonly root: URL;

  /**
   * URL for the levels dir for implicit config or explicit config.
   */
  readonly levels: URL | ReadonlyMap<string, EfcLevelSetConfig>;

  /**
   * Directory containing patcher data files.
   */
  readonly patcherData: URL;

  /**
   * Score items configuration file.
   */
  readonly scoreItems: URL;

  /**
   * Special items configuration file.
   */
  readonly specialItems: URL;

  /**
   * Dimensions configuration file.
   */
  readonly dimensions: URL;

  readonly locales: ReadonlyMap<string, EfcLocaleConfig>;

  readonly assets: EfcAssetsConfig;

  readonly prettyOutput?: boolean;

  /**
   * If defined, enable variable name obfuscation.
   */
  readonly obf?: EfcObfConfig;
}

export interface EfcObfConfig {
  /**
   * Obfuscation key.
   * Use `null` to only perform normalization.
   */
  readonly key: string | null;

  /**
   * Parent map or URL to the JSON file containing it.
   */
  readonly parentMap: URL | ReadonlyMap<string, string>;
}

export interface EfcLocaleConfig {
  readonly lands: URL;
  readonly items: URL;
  readonly statics: URL;
  readonly keys: URL;
}

export interface EfcLevelSetConfig {
  /**
   * Directory containing the level files for this set.
   */
  readonly dir: URL;

  /**
   * Output `did` (used for the tags).
   */
  readonly did?: string;

  /**
   * Clear variable name for this level set.
   */
  readonly varName: string;

  /**
   * Pad the output with an empty level.
   */
  readonly padding: boolean;

  /**
   * Ensure all chunks have the same size, adding padding to shorter chunks.
   */
  readonly equalSize: boolean;
}

export interface EfcAssetsConfig {
  readonly useBaseAssets: boolean;
  readonly roots: ReadonlyArray<URL>;
}

export interface Config {
  readonly outFile: string;
  readonly levelsDir: string;
  readonly patcherDataDir: string;
  readonly scoreItemsFile: string;
  readonly specialItemsFile: string;
  readonly dimensionsFile: string;
  readonly locales: ReadonlyMap<string, LocaleConfig>;
  readonly assets: AssetsConfig;
}

export interface LocaleConfig {
  readonly isDebug: boolean;
  readonly lands: string;
  readonly items: string;
  readonly statics: string;
  readonly keys: string;
}

export interface AssetsConfig {
  readonly useBaseAssets: boolean;
  readonly roots: ReadonlyArray<string>;
}

export interface ResolvedConfig extends Config {
  readonly root: string;
}

export function fromPath(configUri: url.URL): rx.Observable<Cr<ResolvedConfig>> {
  // tslint:disable-next-line:deprecation
  return observeTextFile(configUri).pipe(
    rxOp.map((textCr: Cr<string>): Cr<ResolvedConfig> => {
      return textCr.flatMap((text: string): Cr<ResolvedConfig> => {
        const root: string = sysPath.dirname(toSysPath(configUri.toString()));
        return fromString(text)
          .map(config => resolveConfig(config, root))
          .mapDiagnostics(d => ({...d, fileUri: configUri}));
      });
    }),
    rxOp.share(),
  );
}

function fromString(text: string): Cr<Config> {
  const $: cheerio.Root = cheerio.load(text, {xmlMode: true});
  return readConfig($, $("config"));
}

const DEFAULT_ASSETS: AssetsConfig = {useBaseAssets: true, roots: []};

function readConfig($: cheerio.Root, node: Cheerio): Cr<Config> {
  const langAttributes: Cheerio = node.children("lang_attributes");
  const localeId: Cr<string> = readAttr(langAttributes, "id");
  const debugStr: string | undefined = langAttributes.attr("debug");
  const isDebug: Cr<boolean> = crOf(debugStr === undefined ? false : debugStr !== "0");

  const paths: Cheerio = node.children("paths");
  const outFile: Cr<string> = readAttr(paths.children("path[name=game]"), "path");
  const levelsDir: Cr<string> = readAttr(paths.children("path[name=niveaux]"), "path");
  const patcherDataDir: Cr<string> = readAttr(paths.children("path[name=datas]"), "path");
  const scoreItemsFile: Cr<string> = readAttr(paths.children("path[name=objets_points]"), "path");
  const specialItemsFile: Cr<string> = readAttr(paths.children("path[name=objets_effet]"), "path");
  const levelSetsFile: Cr<string> = readAttr(paths.children("path[name=dimensions]"), "path");
  const assetNodes: Cheerio = node.children("assets");
  let assets: Cr<AssetsConfig>;
  switch (assetNodes.length) {
    case 0:
      assets = crOf(DEFAULT_ASSETS);
      break;
    case 1:
      assets = crOf(readAssetsConfig($, assetNodes));
      break;
    default:
      assets = crError(new AnyError(`InvalidConfig: Expect 0-1 path[name=assets], got ${assetNodes.length}`));
      break;
  }

  const landsLocale: Cr<string> = readAttr(paths.children("path[name=lang_dimensions]"), "path");
  const itemsLocale: Cr<string> = readAttr(paths.children("path[name=lang_objets]"), "path");
  const staticsLocale: Cr<string> = readAttr(paths.children("path[name=lang_statics]"), "path");
  const keysLocale: Cr<string> = readAttr(paths.children("path[name=lang_keys]"), "path");

  const locale: Cr<LocaleConfig> = crJoin({
    isDebug,
    lands: landsLocale,
    items: itemsLocale,
    statics: staticsLocale,
    keys: keysLocale,
  });

  return crJoin({
    outFile,
    levelsDir,
    patcherDataDir,
    scoreItemsFile,
    specialItemsFile,
    dimensionsFile: levelSetsFile,
    locales: crJoinMapEntries([crJoinArray([localeId, locale])]),
    assets,
  });
}

function readAssetsConfig($: cheerio.Root, node: Cheerio): AssetsConfig {
  const useBaseAssets: boolean = node.attr("no-default") !== undefined ? false : DEFAULT_ASSETS.useBaseAssets;
  const roots: string[] = [];
  node.children("asset").each((_i, elem) => {
    roots.push($(elem).text().trim());
  });

  return {useBaseAssets, roots};
}

function resolveConfig(config: Config, root: string): ResolvedConfig {
  const locales: Map<string, LocaleConfig> = new Map();
  for (const [localeId, localeConfig] of config.locales) {
    locales.set(localeId, resolveLocaleConfig(localeConfig, root));
  }

  return {
    root,
    outFile: sysPath.join(root, config.outFile),
    levelsDir: sysPath.join(root, config.levelsDir),
    patcherDataDir: sysPath.join(root, config.patcherDataDir),
    scoreItemsFile: sysPath.join(root, config.scoreItemsFile),
    specialItemsFile: sysPath.join(root, config.specialItemsFile),
    dimensionsFile: sysPath.join(root, config.dimensionsFile),
    locales,
    assets: resolveAssetsConfig(config.assets, root),
  };
}

function resolveLocaleConfig(localeConfig: LocaleConfig, root: string): LocaleConfig {
  return {
    isDebug: localeConfig.isDebug,
    lands: sysPath.join(root, localeConfig.lands),
    items: sysPath.join(root, localeConfig.items),
    statics: sysPath.join(root, localeConfig.statics),
    keys: sysPath.join(root, localeConfig.keys),
  };
}

function resolveAssetsConfig(assetsConfig: AssetsConfig, root: string): AssetsConfig {
  return {
    ...assetsConfig,
    roots: assetsConfig.roots.map(relPath => sysPath.join(root, relPath)),
  };
}
