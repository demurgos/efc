import cheerio from "cheerio";
import Cheerio = cheerio.Cheerio;

export function cheerioToString(root: Cheerio, pretty: boolean = false): string {
  const separator: string = pretty ? "\n" : "";
  const $: cheerio.Root = cheerio.load(`<?xml version="1.0" encoding="UTF-8"?>${separator}`, {xmlMode: true});
  $.root().append(root);
  removeCheerioWhitespace($, root[0] as cheerio.TagElement);
  if (pretty) {
    indentCheerio($, root, 0);
  }
  return `${$.xml()}${separator}`;
}

function removeCheerioWhitespace($: cheerio.Root, node: cheerio.TagElement): void {
  if (node.children === undefined) {
    return;
  }
  if (node.children.length >= 2) {
    const whitespace: cheerio.Element[] = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i: number = 0; i < node.children.length; i++) {
      const child: cheerio.Element = node.children[i];
      if (isWhitespace(child)) {
        whitespace.push(child);
      }
    }
    $(whitespace).remove();
  }
  // tslint:disable-next-line:prefer-for-of
  for (let i: number = 0; i < node.children.length; i++) {
    const child: cheerio.TagElement = node.children[i] as any;
    removeCheerioWhitespace($, child);
  }
}

function isWhitespace(elem: cheerio.Element): boolean {
  return elem.type === "comment" || elem.type === "text" && elem.data !== undefined && /^\s+$/.test(elem.data);
}

function indentCheerio($: cheerio.Root, node: Cheerio, depth: number): void {
  if (depth > 10) {
    return;
  }
  node.children().before(createWhitespace($, depth + 1));
  node.children().last().after(createWhitespace($, depth));
  node.children().each((_, child) => {
    indentCheerio($, $(child), depth + 1);
  });
}

function createWhitespace($: cheerio.Root, depth: number): Cheerio {
  const wrapper: Cheerio = $("<div></div>");
  wrapper.text(`\n${new Array(depth).fill("  ").join("")}`);
  return wrapper.contents();
}
