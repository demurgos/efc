import { LevelRef } from "./compiler/links.mjs";

export interface Land {
  readonly key: string;
  readonly start: LevelRef;
}
