import { CACHE, CacheKey } from "./cache.mjs";
import { computeGridInfo, Grid2D, GridChunkInfo } from "./compiler/grid.mjs";
import { Way } from "./java/dimensions.mjs";
import * as java from "./java/level.mjs";
import { unreachable } from "./utils.mjs";

export type Chunk = LinearChunk | TransientChunk | GridChunk;

export interface LinearChunk {
  readonly type: "linear";
  readonly firstLevelTag?: string;
  readonly levels: ReadonlyArray<java.JavaLevel>;
}

export interface TransientChunk extends CacheKey<TransientChunkInfo> {
  readonly type: "transient";
  readonly children: ReadonlyArray<Chunk>;
}

interface TransientChunkInfo {
  readonly length: number;
}

export interface GridChunk extends CacheKey<GridChunkInfo> {
  readonly type: "grid";
  readonly firstLevelTag: string;
  readonly levels: Grid2D<java.JavaLevel>;
}

export function getChunkLength(chunk: Chunk): number {
  switch (chunk.type) {
    case "linear":
      return chunk.levels.length;
    case "transient": {
      const transient: TransientChunkInfo = CACHE.get(chunk, c => {
        let length: number = 0;
        for (const child of c.children) {
          length += getChunkLength(child);
        }
        return {length};
      });
      return transient.length;
    }
    case "grid": {
      const grid: GridChunkInfo = CACHE.get(chunk, c => computeGridInfo(c.levels));
      return grid.levels.length;
    }
    default:
      return unreachable(chunk, "Unexpected Chunk.type");
  }
}

export function generateWays(chunk: Chunk): ReadonlyArray<Way> {
  switch (chunk.type) {
    case "linear": {
      return [];
    }
    case "transient": {
      const ways: ReadonlyArray<Way>[] = [];
      for (const child of chunk.children) {
        const cur: ReadonlyArray<Way> = generateWays(child);
        if (cur.length > 0) {
          ways.push(cur);
        }
      }
      return Array.prototype.concat.apply([], ways);
    }
    case "grid": {
      const grid: GridChunkInfo = CACHE.get(chunk, c => computeGridInfo(c.levels));
      const tag: string = chunk.firstLevelTag;
      return grid.links.map(link => ({
        type: "two-way" as "two-way",
        from: {
          level: {refType: "grid" as "grid", tag, pos: link.from},
          port: link.fromPort,
        },
        to: {
          level: {refType: "grid" as "grid", tag, pos: link.to},
          port: link.toPort,
        },
      }));
    }
    default:
      return unreachable(chunk, "Unexpected Chunk.type");
  }
}
