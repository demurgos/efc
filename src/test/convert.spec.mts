import * as mtbon from "@eternalfest/mtbon";
import chai from "chai";

import { javaToHfLevel } from "../lib/compiler/convert.mjs";
import * as hf from "../lib/compiler/hf.mjs";
import { CompilerResult as Cr, joinArray as crJoinArray, of as crOf } from "../lib/compiler-result.mjs";
import { DiagnosticCategory } from "../lib/diagnostic/report.mjs";
import { getBaseAssets, JavaAssets } from "../lib/java/assets.mjs";
import * as java from "../lib/java/level.mjs";
import { getSampleLevelsSync } from "./samples/samples.mjs";

function filterWarnings<T>(cr: Cr<T>): Cr<T> {
  return cr.mapDiagnostics(diag => diag.category === DiagnosticCategory.Error ? diag : undefined);
}

describe("convert", () => {
  describe("samples", () => {
    for (const sample of getSampleLevelsSync()) {
      it(sample.name, async () => {
        const expectedHfLevel: Cr<hf.Level> = crOf(sample.hfl);
        const inputLevel: Cr<java.JavaLevel> = java.parseString(sample.lvl);
        const assets: Cr<JavaAssets> = await getBaseAssets();
        const actualHfLevel: Cr<hf.Level> = crJoinArray([inputLevel, assets]).flatMap(vals => {
          const [inputLevel, assets] = vals;
          return javaToHfLevel(inputLevel, assets);
        });
        chai.assert.deepEqual(filterWarnings(actualHfLevel), expectedHfLevel);

        const expectedMtbon: Cr<string> = crOf(sample.mtbon.trim());
        const actualMtbon: Cr<string> = actualHfLevel.map(hfLevel => {
          const escapedHfl: hf.EscapedLevel = hf.escapeLevel(hfLevel);
          return mtbon.emit(escapedHfl);
        });
        chai.assert.deepEqual(filterWarnings(actualMtbon), expectedMtbon);
      });
    }
  });
});
