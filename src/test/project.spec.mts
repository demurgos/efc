import chai from "chai";

import { CompilerResult as Cr, of as crOf } from "../lib/compiler-result.mjs";
import { Compiled, compileFromDirectory } from "../lib/project.mjs";
import { getSampleProjectsSync } from "./samples/samples.mjs";

describe("project", function () {
  describe("compileFromDirectory", function () {
    for (const sample of getSampleProjectsSync()) {
      it(sample.name, async () => {
        const actualCompiled: Cr<Compiled> = await compileFromDirectory(sample.dir, true);
        const actual: Cr<string> = actualCompiled.map(x => x.content);
        const expected: Cr<string> = crOf(sample.expected);
        chai.assert.deepEqual(actual.diagnostics, expected.diagnostics);
        chai.assert.deepEqual(actual.value, expected.value);
      });
    }
  });
});
