import chai from "chai";
import { fromSysPath } from "furi";
import { firstValueFrom } from "rxjs";

import { CompilerResult as Cr, of as crOf } from "../lib/compiler-result.mjs";
import { fromConfig as javaAssetsFromConfig, JavaAssets } from "../lib/java/assets.mjs";
import {
  EfcAssetsConfig,
  fromPath as javaConfigFromPath,
  ResolvedConfig,
} from "../lib/java/config.mjs";
import { ASSETS_CONFIGS_DIR, getSampleAssetsConfigsSync } from "./samples/samples.mjs";

const ASSET_PATH_PREFIX: string = ASSETS_CONFIGS_DIR + "/";

function truncateAssetPath(path: string): string {
  chai.assert(path.startsWith(ASSET_PATH_PREFIX), `invalid asset path: ${path}`);
  return path.substr(ASSET_PATH_PREFIX.length);
}

function copyObj<T extends Record<string, any>>(obj: T, extra: Partial<T>): T {
  const res: any = { ...extra };
  for (const key in obj) {
    if (res[key] === undefined && obj[key] !== undefined) {
      res[key] = obj[key];
    }
  }
  return res;
}

function mapToRecord<V, W>(map: ReadonlyMap<string, V>, func: (val: V) => W): Record<string, W> {
  const obj: Record<string, W> = {};
  for (const [key, value] of map) {
    obj[key] = func(value);
  }
  return obj;
}

function javaAssetsToRawObject(javaAssets: JavaAssets): any {
  return {
    backgrounds: mapToRecord(javaAssets.backgrounds, value => copyObj(value, {
      imageUrl: truncateAssetPath(value.imageUrl),
    })),
    fields: mapToRecord(javaAssets.fields, value => copyObj(value, {
      imageUrl: truncateAssetPath(value.imageUrl),
    })),
    tiles: mapToRecord(javaAssets.tiles, value => copyObj(value, {
      bodyImageUrl: truncateAssetPath(value.bodyImageUrl),
      endImageUrl: truncateAssetPath(value.endImageUrl),
    })),
    bads: mapToRecord(javaAssets.bads, value => copyObj(value, {})),
  };
}

describe("assets", () => {
  describe("samples", function () {
    for (const sample of getSampleAssetsConfigsSync()) {
      it(sample.name, async () => {
        const config: Cr<ResolvedConfig> = await firstValueFrom(javaConfigFromPath(sample.config));
        const assets: Cr<JavaAssets> = await config.flatMapAsync(async config => {
          const efcAssetsConfig: EfcAssetsConfig = {
            useBaseAssets: config.assets.useBaseAssets,
            roots: config.assets.roots.map(fromSysPath),
          };
          return javaAssetsFromConfig(efcAssetsConfig);
        });

        const expectedAssets: Cr<any> = crOf(sample.expected);
        const actualAssets: Cr<any> = assets.map(javaAssetsToRawObject);

        chai.assert.deepEqual(actualAssets, expectedAssets);
      });
    }
  });
});
