# 0.3.1 (2021-10-16)

- **[Fix]** Include `rxjs` patch in published package.

# 0.3.0 (2021-10-16)

- **[Breaking change]** Compile library to ESM, drop `./lib` prefix from deep imports.
- **[Feature]** Improve error messages.
- **[Feature]** Warn on level tags that aren't lowercase.
- **[Fix]** Update dependencies

# 0.2.0 (2020-01-29)

- **[Breaking change]** Update level set configuration.
- **[Feature]** Add support for obfuscation.
- **[Internal]** Fix continuous deployment script.

# 0.1.11 (2019-12-25)

- **[Fix]** Update dependencies.

# 0.1.10 (2019-12-31)

- **[Fix]** Update dependencies.

# 0.1.9 (2019-11-24)

- **[Fix]** Fix issue with dimension separators on level sets with `hasPaddingLevel == false`.

# 0.1.8 (2019-11-20)

- **[Feature]** Add support for multiple lands in Time Attack level sets.
- **[Feature]** Add support for `fjv` level set.
- **[Fix]** Don't emit tags in `game.xml` for unused level sets.

# 0.1.7 (2019-09-05)

- **[Feature]** Add level set signatures.
- **[Fix]** Emit error on invalid asset file.

# 0.1.6 (2019-05-15)

- **[Fix]** Report warning (instead of error) on missing `<player>` tag.

# 0.1.5 (2019-05-15)

- **[Fix]** Add support for levels without `<script>`.
- **[Fix]** Fix crash in `cheerioWriter.removeCheerioWhitespace`.
- **[Fix]** Fix vortex detection for grid chunks.
- **[Fix]** Update dependencies.

# 0.1.4 (2019-04-29)

- **[Feature]** Update dependencies, notably update to `@eternalfest/better-script@2`.
- **[Feature]** Add support for grid chunks.
- **[Feature]** Add support for asset aliases.

# 0.1.3 (2019-02-05)

- **[Fix]** Pretty print `game.xml`.
- **[Fix]** Add support for levels without `<player>`.
- **[Fix]** Fix dimension separator.
- **[Internal]** Refactor `compiler-result` module.

# 0.1.2 (2019-01-23)

- **[Feature]** Add support for advanced tags.
- **[Feature]** Add support for custom assets.
- **[Fix]** Include assets in `lib` builds.

# 0.1.1 (2019-01-08)

- **[Fix]** Fix `--version` command.

# 0.1.0 (2019-01-08)

- **[Feature]** First release.
